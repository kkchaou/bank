package edu.app.persistence;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

/**
 * Entity implementation class for Entity: OfferImpl
 *
 */
@Entity

public class Offer implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1745519919082139023L;
	private int idoffer;
	private String subject;
	private String date_begin;
	private String date_end;
	

	public Offer() {
		super();
	}   
	
	public Offer(String subject, String date_begin, String date_end) {
		super();
		this.subject = subject;
		this.date_begin = date_begin;
		this.date_end = date_end;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public int getIdoffer() {
		return this.idoffer;
	}

	public void setIdoffer(int idoffer) {
		
		int oldidoffer=this.idoffer;
		this.idoffer = idoffer;
		changeSupport.firePropertyChange("idoffer",oldidoffer,idoffer);
		
		
	}   
	public String getSubject() {
		return this.subject;
	}

	public void setSubject(String subject) {
		
		String oldsubject=this.subject;
		this.subject = subject;
		changeSupport.firePropertyChange("subject",oldsubject,subject);
		
	}   
	public String getDate_begin() {
		return this.date_begin;
	}

	public void setDate_begin(String date_begin) {
		
		String olddatebegin = this.date_begin;
		this.date_begin = date_begin;
		changeSupport.firePropertyChange("date_begin",olddatebegin,date_begin);
		
	}
	public String getDate_end() {
		return this.date_end;
	}

	public void setDate_end(String date_end) {
		String olddateend = this.date_end;
		this.date_end = date_end;
		changeSupport.firePropertyChange("date_end",olddateend,date_end);
		
	}
	
	@Transient
	public PropertyChangeSupport getChangeSupport() {
	return changeSupport;
	}
	public void setChangeSupport(PropertyChangeSupport changeSupport) {
	this.changeSupport = changeSupport;
	}
	private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

	public void addPropertyChangeListener(PropertyChangeListener listener) {
	changeSupport.addPropertyChangeListener(listener);
	}
	public void removePropertyChangeListener(PropertyChangeListener listener) {
	changeSupport.removePropertyChangeListener(listener);
	}  
   
}
