package edu.app.persistence;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

//import javax.persistence.ManyToOne;

@Entity
public class Client implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3669441940454543791L;
	private int id;
	private String firstName;
	private String lastName;
	private String address;
	private String tel;
	private String mail;
	private String login;
	private String password;
	private List<Account> accounts;
	private String clienttype;

	public Client() {
	}

	
	
	 public Client(String firstName, String lastName, String address, String tel,
	 String mail) {
	 super();
	 this.firstName = firstName;
	 this.lastName = lastName;
	 this.address = address;
	 this.tel = tel;
	 this.mail = mail;
	 }
	

	public Client(String firstName, String lastName, String address,
			String tel, String mail, String clienttype) {
		super();

		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.tel = tel;
		this.mail = mail;
		this.clienttype = clienttype;
	}

	public Client(String firstName, String lastName, String address,
			String tel, String mail, String login, String password,
			String clienttype) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.tel = tel;
		this.mail = mail;
		this.login = login;
		this.password = password;
		this.clienttype = clienttype;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}
	
	public int returnId() {
		return id;
	}
	

	public void setId(int id) {
		 int oldId = this.id;
		this.id = id;
		 changeSupport.firePropertyChange("id", oldId, id);
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		 String oldFirstname = this.firstName;
		this.firstName = firstName;
		 changeSupport.firePropertyChange("firstName", oldFirstname, firstName);
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		 String oldLastname = this.lastName;
		this.lastName = lastName;
		 changeSupport.firePropertyChange("lastName", oldLastname, lastName);
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		 String oldAddress = this.address;
		this.address = address;
		 changeSupport.firePropertyChange("address", oldAddress, address);
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		 String oldTel = this.tel;
		this.tel = tel;
		 changeSupport.firePropertyChange("tel", oldTel, tel);
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		 String oldMail = this.mail;
		this.mail = mail;
		 changeSupport.firePropertyChange("mail", oldMail, mail);
	}

	@OneToMany(mappedBy = "client", cascade = { CascadeType.PERSIST,
			CascadeType.MERGE }, fetch = FetchType.EAGER)
	public List<Account> getAccounts() {
		return accounts;
	}

	public String getClienttype() {
		
		return clienttype;
	}

	public void setClienttype(String clienttype) {
		String oldClientType = this.clienttype;
		this.clienttype = clienttype;
		changeSupport.firePropertyChange("clienttype", oldClientType, clienttype);
	}

	public void setAccounts(List<Account> accounts) {
		 List<Account> oldAccounts=this.accounts;
		this.accounts = accounts;
		 changeSupport.firePropertyChange("accounts", oldAccounts, accounts);
	}

	

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		 String oldPassword=this.password;
		this.password = password;
		 changeSupport.firePropertyChange("password", oldPassword, password);
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		 String oldLogin=this.login;
		this.login = login;
		 changeSupport.firePropertyChange("login", oldLogin, login);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((accounts == null) ? 0 : accounts.hashCode());
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result
				+ ((clienttype == null) ? 0 : clienttype.hashCode());
		result = prime * result
				+ ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + id;
		result = prime * result
				+ ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		result = prime * result + ((mail == null) ? 0 : mail.hashCode());
		result = prime * result
				+ ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((tel == null) ? 0 : tel.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Client other = (Client) obj;
		if (accounts == null) {
			if (other.accounts != null)
				return false;
		} else if (!accounts.equals(other.accounts))
			return false;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (clienttype != other.clienttype)
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (id != other.id)
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login))
			return false;
		if (mail == null) {
			if (other.mail != null)
				return false;
		} else if (!mail.equals(other.mail))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (tel == null) {
			if (other.tel != null)
				return false;
		} else if (!tel.equals(other.tel))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Client [id=" + id + ", firstName=" + firstName + ", lastName="
				+ lastName + ", address=" + address + ", tel=" + tel
				+ ", mail=" + mail + ", accounts=" + accounts + ", clienttype="
				+ clienttype + "]";
	}

	 @Transient
	 public PropertyChangeSupport getChangeSupport() {
	 return changeSupport;
	 }

	public void setChangeSupport(PropertyChangeSupport changeSupport) {
		this.changeSupport = changeSupport;
	}

	private PropertyChangeSupport changeSupport = new PropertyChangeSupport(
			this);

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		changeSupport.addPropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		changeSupport.removePropertyChangeListener(listener);
	}
	
	
	public void addAccount(Account account){
		this.getAccounts().add(account);
		account.setClient(this);
	}

}
