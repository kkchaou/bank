package edu.app.persistence;



import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;


/**
 * Entity implementation class for Entity: Schedule_credit
 *
 */
@Entity

public class Schedule_credit implements Serializable {

	  private PropertyChangeSupport changesupport = new PropertyChangeSupport(this);
	
	private int idschedule;
	private String date_begin;
	private String date_end;
	private int number_premium;
	private int length_premium;
	private Double amountpremium;
	private List<Credit> credits ;
	private static final long serialVersionUID = 1L;

	public Schedule_credit() {
		super();
	}   

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
	public int getIdschedule() {
		return this.idschedule;
	}

	public void setIdschedule(int idschedule) {
		int oldidschedule = this.idschedule;
		this.idschedule = idschedule;
		changeSupport.firePropertyChange("idschedule", oldidschedule, idschedule);
	}   
	public String getDate_begin() {
		return this.date_begin;
	}

	public void setDate_begin(String date_begin) {
		String olddatebegin = this.date_begin;
		this.date_begin = date_begin;
		changeSupport.firePropertyChange("date_begin", olddatebegin, date_begin);
		
	}   
	public String getDate_end() {
		return this.date_end;
	}

	public void setDate_end(String date_end) {
		String olddateend = this.date_end;
		this.date_end = date_end;
		changeSupport.firePropertyChange("date_end", olddateend, date_end);
	}   
	public int getNumber_premium() {
		return this.number_premium;
	}

	public void setNumber_premium(int number_premium) {
		int oldnumberprem = this.number_premium;
		this.number_premium = number_premium;
		changeSupport.firePropertyChange("idschedule", oldnumberprem, number_premium);
	}   
	public int getLength_premium() {
		return this.length_premium;
	}

	public void setLength_premium(int length_premium) {
		this.length_premium = length_premium;
	}   
	public Double getAmountpremium() {
		return this.amountpremium;
	}

	public void setAmountpremium(Double amountpremium) {
		this.amountpremium = amountpremium;
	}
	
	
	

	@OneToMany(mappedBy="schedule", cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
  public List<Credit> getCredits() {
		return credits;
	}


	public void setCredits(List<Credit> credits) {
		this.credits = credits;
	}
	public void addCredit(Credit credit){
    	this.getCredits().add(credit);
        credit.setSchedule(this);
	}


@Transient
	public PropertyChangeSupport getChangeSupport() {
		return changeSupport;
		}
		public void setChangeSupport(PropertyChangeSupport changeSupport) {
		this.changeSupport = changeSupport;
		}
		private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

		public void addPropertyChangeListener(PropertyChangeListener listener) {
		changeSupport.addPropertyChangeListener(listener);
		}
		public void removePropertyChangeListener(PropertyChangeListener listener) {
		changeSupport.removePropertyChangeListener(listener);
		}

   
}
