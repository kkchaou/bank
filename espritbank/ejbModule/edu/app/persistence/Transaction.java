package edu.app.persistence;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;



/**
 * Entity implementation class for Entity: Transaction
 *
 */
@Entity
public class Transaction implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 2830953780967901354L;
	private int idTransaction;
	private Date transactionDate;
	private String creditDebit;
	private String transactionDescription;
	private float transactionAmount;
	private TransactionType transactionType;
	private Account account;
	private TransactionMean transactionMean;

	
	
	
	

	public Transaction() {
	} 
	
	
	
	public Transaction(Date transactionDate, String creditDebit,
			String transactionDescription, float transactionAmount,
			TransactionType transactionType, Account account,
			TransactionMean transactionMean) {
		this.transactionDate = transactionDate;
		this.creditDebit = creditDebit;
		this.transactionDescription = transactionDescription;
		this.transactionAmount = transactionAmount;
		this.transactionType = transactionType;
		this.account = account;
		this.transactionMean = transactionMean;
	}



	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public int getIdTransaction() {
		return this.idTransaction;
	}

	public void setIdTransaction(int idTransaction) {
		this.idTransaction = idTransaction;
	}   
	public Date getTransactionDate() {
		return this.transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}   
	public String getCreditDebit() {
		return this.creditDebit;
	}

	public void setCreditDebit(String creditDebit) {
		this.creditDebit = creditDebit;
	}   
	public String getTransactionDescription() {
		return this.transactionDescription;
	}

	public void setTransactionDescription(String transactionDescription) {
		this.transactionDescription = transactionDescription;
	}   
	public float getTransactionAmount() {
		return this.transactionAmount;
	}

	public void setTransactionAmount(float transactionAmount) {
		this.transactionAmount = transactionAmount;
	}
	
	
	
	
	public TransactionType getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}

	
	@ManyToOne(cascade=CascadeType.PERSIST)
	public Account getAccount() {
		return account;
	}
	public void setAccount(Account account) {
		this.account = account;
	}

	public TransactionMean getTransactionMean() {
		return transactionMean;
	}

	public void setTransactionMean(TransactionMean transactionMean) {
		this.transactionMean = transactionMean;
	}
	

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((creditDebit == null) ? 0 : creditDebit.hashCode());
		result = prime * result + idTransaction;
		result = prime * result + Float.floatToIntBits(transactionAmount);
		result = prime * result
				+ ((transactionDate == null) ? 0 : transactionDate.hashCode());
		result = prime
				* result
				+ ((transactionDescription == null) ? 0
						: transactionDescription.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Transaction other = (Transaction) obj;
		if (creditDebit == null) {
			if (other.creditDebit != null)
				return false;
		} else if (!creditDebit.equals(other.creditDebit))
			return false;
		if (idTransaction != other.idTransaction)
			return false;
		if (Float.floatToIntBits(transactionAmount) != Float
				.floatToIntBits(other.transactionAmount))
			return false;
		if (transactionDate == null) {
			if (other.transactionDate != null)
				return false;
		} else if (!transactionDate.equals(other.transactionDate))
			return false;
		if (transactionDescription == null) {
			if (other.transactionDescription != null)
				return false;
		} else if (!transactionDescription.equals(other.transactionDescription))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Transaction [idTransaction=" + idTransaction
				+ ", transactionDate=" + transactionDate + ", creditDebit="
				+ creditDebit + ", transactionDescription="
				+ transactionDescription + ", transactionAmount="
				+ transactionAmount + "]";
	}
	
	
   
}
