package edu.app.persistence;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import edu.app.persistence.Transaction;



/**
 * Entity implementation class for Entity: Pos
 *
 */
@Entity
public class Pos implements Serializable {

	   
	
	private int idpos;
	private String address;
	private List<Transaction> transactions;

	private static final long serialVersionUID = 1L;

	public Pos() {
		super();
	}   
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public int getIdpos() {
		return this.idpos;
	
	}

	public void setIdpos(int idpos) {
		
		int oldIdpos=this.idpos;
		this.idpos = idpos;
		changeSupport.firePropertyChange("idpos", oldIdpos, idpos);
	}   
	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		String oldaddress=this.address;
		this.address = address;
		changeSupport.firePropertyChange("address", oldaddress, address);
			}
	
	@OneToMany
		public List<Transaction> getTransactions() {
			return transactions;
	    }
		public void setTransactions(List<Transaction> transactions) {
			this.transactions = transactions;
		}
	

	
	
	@Transient
	 public PropertyChangeSupport getChangeSupport() {
	 return changeSupport;
	 }

	public void setChangeSupport(PropertyChangeSupport changeSupport) {
		this.changeSupport = changeSupport;
	}

	private PropertyChangeSupport changeSupport = new PropertyChangeSupport(
			this);

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		changeSupport.addPropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		changeSupport.removePropertyChangeListener(listener);
	}
   
}
