package edu.app.persistence;


import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 * Entity implementation class for Entity: Credit
 *
 */
@Entity
public class Credit implements Serializable {

	
	  private PropertyChangeSupport changesupport = new PropertyChangeSupport(this);

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4932651619995113339L;
	private int idcredit;
	private Double amountcredit;
	private String type;
    private Schedule_credit schedule;
    
    
	public Credit() {
		super();
	}  
	
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	public int getIdcredit() {
		return this.idcredit;
	}

	public void setIdcredit(int idcredit) {
		
		int oldidcredit = this.idcredit;
		this.idcredit = idcredit;
		changeSupport.firePropertyChange("idcredit", oldidcredit, idcredit);
		
	}   
	public Double getAmountcredit() {
		return this.amountcredit;
	}

	public void setAmountcredit(Double amountcredit) {
		
		Double oldamountcredit = this.amountcredit;
		this.amountcredit = amountcredit;
		changeSupport.firePropertyChange("amountcredit", oldamountcredit, amountcredit);
		
	}
	
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		String oldtype = this.type;
		this.type = type;
		changeSupport.firePropertyChange("type", oldtype, type);
	}

	@ManyToOne 
	@JoinColumn( name = "schedule_fk" )
	public Schedule_credit getSchedule() {
		return schedule;
	}

	public void setSchedule(Schedule_credit schedule) {
		Schedule_credit oldschedule=this.schedule;
		this.schedule = schedule;
		changeSupport.firePropertyChange("schedule", oldschedule, schedule);
	}
	
   
	@Override
	public String toString() {
		return "Credit [idcredit=" + idcredit + ", amountcredit="
				+ amountcredit + ", type=" + type + ", schedule=" + schedule
				+ "]";
	}

	@Transient
	public PropertyChangeSupport getChangeSupport() {
	return changeSupport;
	}
	public void setChangeSupport(PropertyChangeSupport changeSupport) {
	this.changeSupport = changeSupport;
	}
	private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

	public void addPropertyChangeListener(PropertyChangeListener listener) {
	changeSupport.addPropertyChangeListener(listener);
	}
	public void removePropertyChangeListener(PropertyChangeListener listener) {
	changeSupport.removePropertyChangeListener(listener);
	}
   
}
