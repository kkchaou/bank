package edu.app.persistence;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Account implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7520084845399994226L;
	private int accountNumber;
	private Date openingDate;
	private float balance;
	private Client client;
	private String accountType;
	private CreditCard creditCard;
	private List<Transaction> transactions;
	
	
	
	public Account() {
	}

	


	public Account(float balance, Client client) {
		super();
		this.balance = balance;
		this.client = client;
	}
	
	


	public Account(int accountNumber, float balance, Client client) {
		super();
		this.accountNumber = accountNumber;
		this.balance = balance;
		this.client = client;
	}




	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public int getAccountNumber() {
		return accountNumber;
	}



	@ManyToOne
	public Client getClient() {
		return client;
	}



	public void setClient(Client client) {
		this.client = client;
	}



	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}


@Temporal(TemporalType.DATE)
	public Date getOpeningDate() {
		return openingDate;
	}



	public void setOpeningDate(Date openingDate) {
		this.openingDate = openingDate;
	}



	public float getBalance() {
		return balance;
	}



	public void setBalance(float balance) {
		this.balance = balance;
	}


	
	@OneToMany(mappedBy="account",cascade={CascadeType.PERSIST,CascadeType.MERGE},fetch=FetchType.LAZY)
		public List<Transaction> getTransactions() {
		return transactions;
	}



	public void setTransactions(List<Transaction> transactions) {
		this.transactions = transactions;
	}
	
	


	public String getAccountType() {
		return accountType;
	}




	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}


	public int returnAccNumber() {
		return accountNumber;
	}

    

	




	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + accountNumber;
		result = prime * result + Float.floatToIntBits(balance);
		result = prime * result
				+ ((openingDate == null) ? 0 : openingDate.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		if (accountNumber != other.accountNumber)
			return false;
		if (Float.floatToIntBits(balance) != Float
				.floatToIntBits(other.balance))
			return false;
		if (openingDate == null) {
			if (other.openingDate != null)
				return false;
		} else if (!openingDate.equals(other.openingDate))
			return false;
		return true;
	}



	@Override
	public String toString() {
		return "Account [accountNumber=" + accountNumber + ", openingDate="
				+ openingDate + ", balance=" + balance + "]";
	}
	
	public String viewBalance(){
		return "Your balance is : "+balance;
	}




	@OneToOne(mappedBy="account")
	public CreditCard getCreditCard() {
		return creditCard;
	}




	public void setCreditCard(CreditCard creditCard) {
		this.creditCard = creditCard;
	}



 
	
	
	
	
	

}
