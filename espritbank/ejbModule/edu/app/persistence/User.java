package edu.app.persistence;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: user
 *
 */
@Entity
@NamedQuery( name = "tarara", query = "select u from User u")
public class User implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2653037428954127794L;
	private int idUser;
	private String firstname;
	private String lastname;
	private String tel;
	private String mail;
	private UserRole userRole;
	private String login;
	private String password;
	

	public User() {
		super();
	} 
	
	
	
	
	public User(String firstname, String lastname, String tel, String mail,
			UserRole userRole, String login, String password) {
		super();
		this.firstname = firstname;
		this.lastname = lastname;
		this.tel = tel;
		this.mail = mail;
		this.userRole = userRole;
		this.login = login;
		this.password = password;
	}




	@Id  
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public int getIdUser() {
		return this.idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}   
	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}   
	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}   
	public String getTel() {
		return this.tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}   
	public String getMail() {
		return this.mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}  
	
	@Enumerated(EnumType.STRING)
	public UserRole getUserRole() {
		return userRole;
	}
	public void setUserRole(UserRole userRole) {
		this.userRole = userRole;
	}
	public String getLogin() {
		return this.login;
	}

	public void setLogin(String login) {
		this.login = login;
	}   
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
   
}
