package edu.app.persistence;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: CreditCard
 *
 */
@Entity

public class CreditCard implements Serializable {

	
	private int idCard;
	private String numCard;
	private String codeCard;
	private Account account;
	private static final long serialVersionUID = 1L;

	public CreditCard() {
		super();
	}   
	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public int getIdCard() {
		return this.idCard;
	}

	public void setIdCard(int idCard) {
		this.idCard = idCard;
	}   
	public String getNumCard() {
		return this.numCard;
	}

	public void setNumCard(String numCard) {
		this.numCard = numCard;
	}   
	public String getCodeCard() {
		return this.codeCard;
	}

	public void setCodeCard(String codeCard) {
		this.codeCard = codeCard;
	}
	@OneToOne
	//@Column(name="account_number")
	public Account getAccount() {
		return account;
	}
	@Override
	public String toString() {
		return "CreditCard [idCard=" + idCard + ", numCard=" + numCard
				+ ", codeCard=" + codeCard + ", account=" + account + "]";
	}
	public void setAccount(Account account) {
		this.account = account;
	}
   
}
