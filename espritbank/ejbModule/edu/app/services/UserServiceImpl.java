package edu.app.services;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import edu.app.persistence.User;

/**
 * Session Bean implementation class UserServiceImpl
 */
@Stateless
public class UserServiceImpl implements UserServiceRemote, UserServiceLocal {

	/**
	 * Default constructor.
	 */

	@PersistenceContext(unitName = "PU")
	EntityManager em;

	public UserServiceImpl() {
	}

	@Override
	public void createUser(User user) {
		em.persist(user);

	}

	@Override
	public User findUserById(int id) {
		return null;
	}

	@Override
	public void updateUser(User user) {

	}

	@Override
	public void removeUser(User user) {
		em.remove(em.merge(user));
	}

	@Override
	public List<User> findUserByFirstname(String firstname) {
		return null;
	}

	@Override
	public List<User> findUserByLastname(String lastname) {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> findAllUsers() {
		return em.createQuery("select u from User u").getResultList();
	}

	
	@Override
	public boolean authenticate(String login, String password) {

		User u = new User();
		UserServiceImpl us = new UserServiceImpl();
		List<User> list = new ArrayList<User>();
		list = us.findAllUsers();
		boolean trouve = false;
		for (@SuppressWarnings("unused") User usr : list) {
			if (u.getLogin().equals(login) && u.getPassword().equals(password)) {
				trouve = true;
				break;
			}

		}

		return trouve;

	}

}
