package edu.app.services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import edu.app.persistence.Account;

/**
 * Session Bean implementation class AccountServiceImpl
 */
@Stateless
@LocalBean
public class AccountServiceImpl implements AccountServiceRemote {
	
	@PersistenceContext(unitName = "PU")
	private EntityManager em;

    /**
     * Default constructor. 
     */
    public AccountServiceImpl() {
    }

	@Override
	public void createAccount(Account account) {
		em.persist(account);
		
	}

	@Override
	public void updateAccount(Account account) {
		em.merge(account);
		
	}

	@Override
	public void removeAccount(Account account) {
		em.remove(em.merge(account));
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Account> findAllIdAccounts() {
		return em.createQuery("select a from Account a where a.accountType = 'currentAccount'").getResultList();
	}

	


}
