package edu.app.services;

import java.util.List;

import javax.ejb.Remote;

import edu.app.persistence.Pos;


@Remote
public interface PosRemote {
	
	void createpos(Pos p);
	void updatepos(Pos p);
	void deletepos(Pos p);
	Pos findposbyid(int idpos);
	List<Pos>findallpos();

}
