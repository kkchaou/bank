package edu.app.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import edu.app.persistence.Schedule_credit;

/**
 * Session Bean implementation class Schedule_creditImpl
 */
@Stateless
public class Schedule_creditImpl implements Schedule_creditRemote, Schedule_creditLocal {
	@PersistenceContext(unitName ="PU")
	private EntityManager em;

    /**
     * Default constructor. 
     */
    public Schedule_creditImpl() {
       
    }

	@Override
	public void createschedule(Schedule_credit schedule) {
		em.persist(schedule);
		
	}

	@Override
	public void updateschedule(Schedule_credit schedule) {
		em.merge(schedule);
		
	}

	@Override
	public void deleteschedule(Schedule_credit schedule) {
		em.remove(em.merge(schedule));
		
	}

	@Override
	public Schedule_credit findschedulebyid(int idschedule) {
		return em.find(Schedule_credit.class, idschedule);
	}

	@SuppressWarnings({ "unchecked" })
	@Override
	public List<Schedule_credit> findallschedule() {
		return em.createQuery("select sc from Schedule_credit sc").getResultList();
	}

}
