package edu.app.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


import edu.app.persistence.Pos;

/**
 * Session Bean implementation class PosImpl
 */
@Stateless
public class PosImpl implements PosRemote, PosLocal {
	@PersistenceContext(unitName="PU")
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public PosImpl() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void createpos(Pos p) {
          em.persist(p);		
	}

	@Override
	public void updatepos(Pos p) {
         em.merge(p);		
	}

	@Override
	public void deletepos(Pos p) {
		em.remove(em.merge(p));
		
	}

	@Override
	public Pos findposbyid(int idpos) {
		return em.find(Pos.class, idpos);

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Pos> findallpos() {
		return em.createQuery("select p from Pos p").getResultList();

	}

}
