package edu.app.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import edu.app.persistence.Offer;

/**
 * Session Bean implementation class Offer
 */
@Stateless
public class OfferImpl implements OfferRemote, OfferLocal {
	
	@PersistenceContext(unitName="PU")
    private EntityManager em;
    /**
     * Default constructor. 
     */
    public OfferImpl() {
       
    }
	@Override
	public void createoffer(Offer offer) {
		em.persist(offer);
		
	}
	@Override
	public void updateoffer(Offer offer) {
	 em.merge(offer);
		
	}
	@Override
	public void deleteoffer(Offer offer) {
		em.remove(em.merge(offer));
		
	}
	@Override
	public Offer findofferbyid(int idoffer) {
		return em.find(Offer.class, idoffer);
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<Offer> findalloffer() {
		return em.createQuery("select o from Offer o").getResultList();
	}

}
