package edu.app.services;

import java.util.List;

import javax.ejb.Remote;

import edu.app.persistence.Credit;

@Remote
public interface CreditRemote {
	
	void createcredit(Credit credit);
	void updatecredit(Credit credit);
	void deletecredit(Credit credit);
	Credit findcreditbyid(int idcredit);
	List<Credit>findallcredit();

}
