package edu.app.services;

import javax.ejb.Remote;

import edu.app.persistence.Transaction;

@Remote
public interface TransactionServiceRemote {
	void createTransaction(Transaction transaction);
	Transaction findTransactionByDate(int id);
	Transaction findTransactionByAmount(float amount);
	Transaction findTransactionByAccount(int accountNumber);
	
}
