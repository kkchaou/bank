package edu.app.services;

import javax.ejb.Remote;

import edu.app.persistence.Account;
import edu.app.persistence.CreditCard;

@Remote
public interface CreditCardServiceRemote {
	public void createCreditCard(CreditCard creditCard);
	public void removeCreditCard(CreditCard creditCard);
	public String generateCreditCardNumber();
	public CreditCard findCreditCardByNumber(String number);
	public  Account findAccountByCreditCard(CreditCard creditcard);
	

}
