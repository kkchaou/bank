package edu.app.services;

import java.util.List;

import javax.ejb.Remote;

import edu.app.persistence.Client;

@Remote
public interface ClientServiceRemote {
	
	void createClient(Client client);
	Client findClientById(int id);
	Client findClientByLogin(String login);
	Client findClientByName(String firstname,String lastname,String mail);
	void updateClient(Client client);
	void removeClient(Client client);
	List<Client> findClientByFirstname(String firstname);
	List<Client> findClientByLastname(String lastname);
	List<Client> findClientByAddress(String address);
	List<Client> findAll();
	Client find(String mail);
	List<Client> findAllIdClients();

}
