package edu.app.services;

import java.util.List;

import javax.ejb.Remote;

import edu.app.persistence.User;

@Remote
public interface UserServiceRemote {
	
	void createUser(User user);
	User findUserById(int id);
	void updateUser(User user);
	void removeUser(User user);
	boolean authenticate(String login,String password);
	List<User> findUserByFirstname(String firstname);
	List<User> findUserByLastname(String lastname);
	List<User> findAllUsers();



}
