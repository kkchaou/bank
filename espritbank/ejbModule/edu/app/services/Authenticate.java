package edu.app.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import edu.app.persistence.CreditCard;
import edu.app.persistence.User;
import edu.app.persistence.UserRole;

/**
 * Session Bean implementation class Authentication
 */
@Stateless
public class Authenticate implements AuthenticateRemote {

	@PersistenceContext(unitName = "PU")
	private EntityManager em;

	/**
	 * Default constructor.
	 */
	public Authenticate() {

	}

	@Override
	public void createUser(User user) {
		em.persist(user);

	}

	@Override
	public User authenticate(String login, String password) {
		String jpql = "select u from User u where u.login=:login and u.password=:password";
		User found = null;
		Query query = em.createQuery(jpql);
		query.setParameter("login", login);
		query.setParameter("password", password);
		try {
			found = (User) query.getSingleResult();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return found;
	}

	@Override
	public User authenticateBanker(String login, String password) {
		String jpql = "select u from User u where u.login=:login and u.password=:password and u.userRole=:userRole";
		User found = null;
		Query query = em.createQuery(jpql);
		query.setParameter("login", login);
		query.setParameter("password", password);
		query.setParameter("userRole", UserRole.banker);
		try {
			found = (User) query.getSingleResult();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return found;
	}

	@Override
	public User authenticateAdmin(String login, String password) {
		String jpql = "select u from User u where u.login=:login and u.password=:password and u.userRole=:userRole";
		User found = null;
		Query query = em.createQuery(jpql);
		query.setParameter("login", login);
		query.setParameter("password", password);
		query.setParameter("userRole", UserRole.administrator);
		try {
			found = (User) query.getSingleResult();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return found;
	}
	
	
	@Override
	public CreditCard authenticateCreditCard(String numCard, String codeCard) {
		String jpql = "select c from CreditCard c where c.numCard=:numCard and c.codeCard=:codeCard";
		CreditCard found=null;
		Query query = em.createQuery(jpql);
		query.setParameter("numCard", numCard);
		query.setParameter("codeCard", codeCard);
		try {
			found = (CreditCard) query.getSingleResult();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return found;
	}
	
	

	
	

	@SuppressWarnings("unchecked")
	@Override
	public List<User> findAllUsers() {

		return em.createNamedQuery("tarara").getResultList();
	}

	
	
	// @SuppressWarnings("unchecked")
	// public List<User> findAllBankers() {
	//
	// return em.createQuery("select e from Employee e").getResultList();
	// }

}
