package edu.app.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import edu.app.persistence.Client;

/**
 * Session Bean implementation class ClientDAOImpl
 */

@Stateless
public class ClientServiceImpl implements ClientServiceRemote {

	@PersistenceContext(unitName = "PU")
	private EntityManager em;

	/**
	 * Default constructor.
	 */
	public ClientServiceImpl() {
	}

	@Override
	public void createClient(Client client) {
		em.persist(client);

	}

	@Override
	public Client findClientById(int id) {
		return em.find(Client.class, id);
	}

	@SuppressWarnings("unchecked")
	public List<Client> findClientByFirstname(String firstname) {
		return em
				.createQuery(
						"select c from Client c where c.firstName like :param  ")
				.setParameter("param", "%" + firstname + "%").getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Client> findClientByLastname(String lastname) {
		return em
				.createQuery(
						"select c from Client c where c.lastName like :param  ")
				.setParameter("param", "%" + lastname + "%").getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Client> findClientByAddress(String address) {
		return em
				.createQuery(
						"select c from Client c where c.address like :param  ")
				.setParameter("param", "%" + address + "%").getResultList();
	}

	@Override
	public void updateClient(Client client) {
		em.merge(client);

	}

	@Override
	public void removeClient(Client client) {
		em.remove(em.merge(client));

	}

	@SuppressWarnings("unchecked")
	public List<Client> findAll() {
		return em.createQuery("select c from Client c").getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Client findClientByLogin(String login) {
		String jpql = "select c from Client c where c.login=:login";
		Client found = null;
		Query query = em.createQuery(jpql);
		query.setParameter("login", login);
		try {
			found = (Client) query.getSingleResult();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return found;
	}
	
	@Override
	public Client find(String mail) {

		String jpql = "select c from Client c where c.mail=:mail";
		Client found = null;
		Query query = em.createQuery(jpql);
		query.setParameter("mail", mail);
		try {
			found = (Client) query.getSingleResult();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return found;
	}
	
	
	
	
	
	
	
	

	@Override
	public Client findClientByName(String firstname, String lastname,
			String mail) {
		// TODO Auto-generated method stub
		return null;
	}

	
	
	@SuppressWarnings("unchecked")
	public List<Client> findAllIdClients() {
		return em.createQuery("select c from Client c").getResultList();
	}


	/*
	 * @SuppressWarnings("unchecked") public Object findClientByName(String
	 * firstname, String lastname, String mail) {
	 * 
	 * return em.createNativeQuery("select * from Client c where c.firstname ='"
	 * + firstname + "' and c.lastname ='" + lastname + "' and c.mail ='" + mail
	 * + "'").getSingleResult(); // return em.createNativeQuery( //
	 * "select * from Client c where c.firstname ='" + firstname // +
	 * "' and c.lastname ='" + lastname + "' and c.mail ='" // + mail +
	 * "'").getResultList(); }
	 */
}
