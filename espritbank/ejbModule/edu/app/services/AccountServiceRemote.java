package edu.app.services;

import java.util.List;

import javax.ejb.Remote;

import edu.app.persistence.Account;

@Remote
public interface AccountServiceRemote {
	
	void createAccount(Account account);
	List<Account> findAllIdAccounts();
	//Client findClientById(int id);
	//List<Client> findClientByLogin(String login);
	//Client findClientByName(String firstname,String lastname,String mail);
	void updateAccount(Account account);
	void removeAccount(Account account);
	//CreditCard findC
	//void viewBalance(Account account);
//	List<Client> findClientByFirstname(String firstname);
//	List<Client> findClientByLastname(String lastname);
//	List<Client> findClientByAddress(String address);
//	List<Client> findAll();
//	Client find(String mail);

}
