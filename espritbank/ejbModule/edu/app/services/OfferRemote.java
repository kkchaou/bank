package edu.app.services;

import java.util.List;

import javax.ejb.Remote;

import edu.app.persistence.Offer;

@Remote
public interface OfferRemote {

	void createoffer(Offer offer);
	void updateoffer(Offer offer);
	void deleteoffer(Offer offer);
	Offer findofferbyid(int idoffer);
	List<Offer>findalloffer();

}
