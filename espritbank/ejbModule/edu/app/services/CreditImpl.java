package edu.app.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import edu.app.persistence.Credit;

/**
 * Session Bean implementation class CreditImpl
 */
@Stateless
public class CreditImpl implements CreditRemote, CreditLocal {
	@PersistenceContext(unitName="PU")
	EntityManager em;

    /**
     * Default constructor. 
     */
    public CreditImpl() {
        
    }

	@Override
	public void createcredit(Credit credit) {
		em.persist(credit);
		
	}

	@Override
	public void updatecredit(Credit credit) {
		em.merge(credit);
		
	}

	@Override
	public void deletecredit(Credit credit) {
		em.remove(em.merge(credit));
		
	}

	@Override
	public Credit findcreditbyid(int idcredit) {
		return em.find(Credit.class, idcredit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Credit> findallcredit() {
		return em.createQuery("select c from Credit c").getResultList();
	}



}
