package edu.app.services;

import java.util.List;

import javax.ejb.Remote;

import edu.app.persistence.Schedule_credit;

@Remote
public interface Schedule_creditRemote {
	
	void createschedule(Schedule_credit schedule);
	void updateschedule(Schedule_credit schedule);
	void deleteschedule(Schedule_credit schedule);
	Schedule_credit findschedulebyid(int idschedule);
	List<Schedule_credit>findallschedule();


}
