package edu.app.services;

import java.util.List;

import javax.ejb.Remote;

import edu.app.persistence.CreditCard;
import edu.app.persistence.User;


@Remote
public interface AuthenticateRemote {
	void createUser(User user);
	List<User> findAllUsers();
	User authenticate(String login, String password);
	User authenticateBanker(String login, String password);
	User authenticateAdmin(String login, String password);
	CreditCard authenticateCreditCard(String numCard,String codeCard);
	//List<User> findAllBankers();
}
