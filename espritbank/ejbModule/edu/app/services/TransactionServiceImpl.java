package edu.app.services;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import edu.app.persistence.Transaction;

/**
 * Session Bean implementation class TransactionServiceImpl
 */
@Stateless
public class TransactionServiceImpl implements TransactionServiceRemote, TransactionServiceLocal {

    /**
     * Default constructor.
     */
	@PersistenceContext(unitName="PU")
	private EntityManager em;
    public TransactionServiceImpl() {
    	
    	
    }

	@Override
	public void createTransaction(Transaction transaction) {
	em.persist(em.merge(transaction));
	}

	@Override
	public Transaction findTransactionByDate(int idTransaction) {
		return em.find(Transaction.class, idTransaction);
	}

	@Override
	public Transaction findTransactionByAmount(float amount) {
		return null;
	}

	@Override
	public Transaction findTransactionByAccount(int accountNumber) {
		return null;
	}

}
