package edu.app.services;

import java.util.Random;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import edu.app.persistence.Account;
import edu.app.persistence.CreditCard;

/**
 * Session Bean implementation class CreditCardServiceImpl
 */
@Stateless
public class CreditCardServiceImpl implements CreditCardServiceRemote {
	@PersistenceContext(unitName = "PU")
	EntityManager em;

    /**
     * Default constructor. 
     */
    public CreditCardServiceImpl() {
    	
    }

	
	public void createCreditCard(CreditCard creditCard) {
		em.persist(creditCard);
	}

	public void removeCreditCard(CreditCard creditCard) {
		em.remove(creditCard);
	}
	
	
	public String generateCreditCardNumber() {
		String alphabet = "1234567890";
		String ccn = "";
		boolean existingcard = true;
		
		while (existingcard)
		{
			ccn = "";
			Random rand = new Random();
			for (int i = 0; i < 10; i++) 
				{
					ccn = ccn + alphabet.charAt(rand.nextInt(alphabet.length()));
				}
			//IF ccn n'existe pas dans la BD
			if(findCreditCardByNumber(ccn) == null)
			{
				existingcard = false;
			}
		
		}

		return ccn;
	}


	@Override
	public CreditCard findCreditCardByNumber(String number) {
		CreditCard cc=null;
		try{
		cc= (CreditCard)em
				.createQuery(
						"select cc from CreditCard cc where cc.numCard = :param  ")
				.setParameter("param", number).getSingleResult();
		return cc;
		}
		catch(NoResultException e)
		{
			return null;
		}
	}


	
	public Account findAccountByCreditCard(CreditCard creditcard) {
		CreditCard creditCard2 =(CreditCard) em
				.createQuery("select C from CreditCard c  where c.idCard=:p").setParameter("p", creditcard.getIdCard()).getSingleResult();
	
		
		return creditCard2.getAccount();
	}


	


}
