package edu.app.GUI;

import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Schedules;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.observablecollections.ObservableCollections;
import org.jdesktop.swingbinding.JTableBinding;
import org.jdesktop.swingbinding.SwingBindings;

import edu.app.delegate.Schedule_creditServiceDelegate;
import edu.app.persistence.Schedule_credit;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.UIManager;
import java.awt.Color;
import org.jdesktop.beansbinding.AutoBinding;
import org.jdesktop.beansbinding.Bindings;
import java.awt.Font;
public class ScheduleManagementForm extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	List<Schedule_credit> schedules;

	private JPanel contentPane;
	private JTextField txtdatebegin;
	private JTextField txtdateend;
	private JTextField txtnumberpremium;
	private JTextField txtlengthpremium;
	private JTextField txtamountpremium;
	private JTable mastertablesc;
	private JTextField textidschedule;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ScheduleManagementForm frame = new ScheduleManagementForm();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ScheduleManagementForm() {
		
		schedules = ObservableCollections.observableList(Schedule_creditServiceDelegate.findallschedule()) ;
		setTitle("Schedule Management");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 492, 601);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Schedules", TitledBorder.LEADING, TitledBorder.TOP, null, Color.BLUE));
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Schedule Details", TitledBorder.LEADING, TitledBorder.TOP, null, Color.BLUE));
		
		JPanel panel_2 = new JPanel();
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(panel, GroupLayout.DEFAULT_SIZE, 434, Short.MAX_VALUE)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(18)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 438, Short.MAX_VALUE)
								.addComponent(panel_2, GroupLayout.DEFAULT_SIZE, 416, Short.MAX_VALUE))))
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 273, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(panel_2, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
		);
		
		JButton btnNewButton_1 = new JButton("Delete");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int[] selected = mastertablesc.getSelectedRows();
				List<Schedule_credit> toremove =new ArrayList<Schedule_credit>(selected.length);
				for(int i=0;i<selected.length;i++){
					Schedule_credit scedul = schedules.get(mastertablesc.convertRowIndexToModel(i));
					toremove.add(scedul);
				}
				for(Schedule_credit scd : toremove){
					Schedule_creditServiceDelegate.deleteschedule(scd);
				}
				schedules.removeAll(toremove);
			}
			
		});
		
		JButton btnNewButton_2 = new JButton("Save");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				for (Schedule_credit sc : schedules)
					Schedule_creditServiceDelegate.updateschedule(sc);
			}
		});
		
		JButton btnNewButton_3 = new JButton("Load");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				List<Schedule_credit> loaded =Schedule_creditServiceDelegate.findallschedule();
				schedules.clear();
				schedules.addAll(loaded);
			
			}
		});
		
		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		
		JButton btnNew = new JButton("New");
		btnNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Schedule_credit schedule = new Schedule_credit();
				schedules.add(schedule);
				int row = schedules.size() - 1;
				mastertablesc.setRowSelectionInterval(row, row);
				mastertablesc.scrollRectToVisible(mastertablesc.getCellRect(row, 0, true));
			}
		});
		GroupLayout gl_panel_2 = new GroupLayout(panel_2);
		gl_panel_2.setHorizontalGroup(
			gl_panel_2.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addContainerGap(115, Short.MAX_VALUE)
					.addComponent(btnNew, GroupLayout.PREFERRED_SIZE, 63, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(btnNewButton_1)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnNewButton_2)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnNewButton_3)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnExit, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
					.addGap(2))
		);
		gl_panel_2.setVerticalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnExit)
						.addComponent(btnNewButton_3)
						.addComponent(btnNewButton_2)
						.addComponent(btnNewButton_1)
						.addComponent(btnNew))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		panel_2.setLayout(gl_panel_2);
		
		JLabel lblNewLabel = new JLabel("Date Beginning");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 10));
		
		JLabel lblNewLabel_1 = new JLabel("Date End");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 10));
		
		JLabel lblNewLabel_2 = new JLabel("Number Premium");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 10));
		
		JLabel lblNewLabel_3 = new JLabel("Length Premium");
		lblNewLabel_3.setFont(new Font("Tahoma", Font.BOLD, 10));
		
		txtdatebegin = new JTextField();
		txtdatebegin.setColumns(10);
		
		txtdateend = new JTextField();
		txtdateend.setColumns(10);
		
		txtnumberpremium = new JTextField();
		txtnumberpremium.setColumns(10);
		
		txtlengthpremium = new JTextField();
		txtlengthpremium.setColumns(10);
		
		JLabel lblNewLabel_4 = new JLabel("Amount Premium");
		lblNewLabel_4.setFont(new Font("Tahoma", Font.BOLD, 10));
		
		txtamountpremium = new JTextField();
		txtamountpremium.setColumns(10);
		
		JLabel lblIdSchedule = new JLabel("Id Schedule");
		lblIdSchedule.setFont(new Font("Tahoma", Font.BOLD, 10));
		
		textidschedule = new JTextField();
		textidschedule.setColumns(10);
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addComponent(lblNewLabel)
						.addComponent(lblNewLabel_1)
						.addComponent(lblNewLabel_2)
						.addComponent(lblIdSchedule)
						.addComponent(lblNewLabel_3)
						.addComponent(lblNewLabel_4))
					.addGap(38)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addComponent(txtamountpremium, GroupLayout.DEFAULT_SIZE, 288, Short.MAX_VALUE)
						.addComponent(txtlengthpremium, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 288, Short.MAX_VALUE)
						.addComponent(textidschedule, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 288, Short.MAX_VALUE)
						.addComponent(txtdateend, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 288, Short.MAX_VALUE)
						.addComponent(txtdatebegin, GroupLayout.DEFAULT_SIZE, 288, Short.MAX_VALUE)
						.addComponent(txtnumberpremium, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 288, Short.MAX_VALUE))
					.addContainerGap())
		);
		gl_panel_1.setVerticalGroup(
			gl_panel_1.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(textidschedule, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblIdSchedule))
					.addPreferredGap(ComponentPlacement.RELATED, 19, Short.MAX_VALUE)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(txtdatebegin, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNewLabel))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(txtdateend, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNewLabel_1))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_2)
						.addComponent(txtnumberpremium, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(txtlengthpremium, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNewLabel_3))
					.addGap(18)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_4)
						.addComponent(txtamountpremium, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(17))
		);
		panel_1.setLayout(gl_panel_1);
		
		JScrollPane scrollPane = new JScrollPane();
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 434, Short.MAX_VALUE))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 246, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		
		mastertablesc = new JTable();
		scrollPane.setViewportView(mastertablesc);
		panel.setLayout(gl_panel);
		contentPane.setLayout(gl_contentPane);
		initDataBindings();
	}
	protected void initDataBindings() {
		JTableBinding<Schedule_credit, List<Schedule_credit>, JTable> jTableBinding = SwingBindings.createJTableBinding(UpdateStrategy.READ_WRITE, schedules, mastertablesc);
		//
		BeanProperty<Schedule_credit, Integer> schedule_creditBeanProperty = BeanProperty.create("idschedule");
		jTableBinding.addColumnBinding(schedule_creditBeanProperty).setColumnName("ID Schedule");
		//
		BeanProperty<Schedule_credit, String> schedule_creditBeanProperty_1 = BeanProperty.create("date_begin");
		jTableBinding.addColumnBinding(schedule_creditBeanProperty_1).setColumnName("Date Beginning");
		//
		BeanProperty<Schedule_credit, String> schedule_creditBeanProperty_2 = BeanProperty.create("date_end");
		jTableBinding.addColumnBinding(schedule_creditBeanProperty_2).setColumnName("Date End");
		//
		BeanProperty<Schedule_credit, Integer> schedule_creditBeanProperty_3 = BeanProperty.create("number_premium");
		jTableBinding.addColumnBinding(schedule_creditBeanProperty_3).setColumnName("Number Premium");
		//
		BeanProperty<Schedule_credit, Integer> schedule_creditBeanProperty_4 = BeanProperty.create("length_premium");
		jTableBinding.addColumnBinding(schedule_creditBeanProperty_4).setColumnName("Length Premium");
		//
		BeanProperty<Schedule_credit, Double> schedule_creditBeanProperty_5 = BeanProperty.create("amountpremium");
		jTableBinding.addColumnBinding(schedule_creditBeanProperty_5).setColumnName("Amount Premium");
		//
		jTableBinding.bind();
		//
		BeanProperty<JTable, Double> jTableBeanProperty = BeanProperty.create("selectedElement.amountpremium");
		BeanProperty<JTextField, String> jTextFieldBeanProperty = BeanProperty.create("text");
		AutoBinding<JTable, Double, JTextField, String> autoBinding = Bindings.createAutoBinding(UpdateStrategy.READ_WRITE, mastertablesc, jTableBeanProperty, txtamountpremium, jTextFieldBeanProperty);
		autoBinding.bind();
		//
		BeanProperty<JTable, Integer> jTableBeanProperty_1 = BeanProperty.create("selectedElement.length_premium");
		BeanProperty<JTextField, String> jTextFieldBeanProperty_1 = BeanProperty.create("text");
		AutoBinding<JTable, Integer, JTextField, String> autoBinding_1 = Bindings.createAutoBinding(UpdateStrategy.READ_WRITE, mastertablesc, jTableBeanProperty_1, txtlengthpremium, jTextFieldBeanProperty_1);
		autoBinding_1.bind();
		//
		BeanProperty<JTable, Integer> jTableBeanProperty_2 = BeanProperty.create("selectedElement.number_premium");
		BeanProperty<JTextField, String> jTextFieldBeanProperty_2 = BeanProperty.create("text");
		AutoBinding<JTable, Integer, JTextField, String> autoBinding_2 = Bindings.createAutoBinding(UpdateStrategy.READ_WRITE, mastertablesc, jTableBeanProperty_2, txtnumberpremium, jTextFieldBeanProperty_2);
		autoBinding_2.bind();
		//
		BeanProperty<JTable, String> jTableBeanProperty_3 = BeanProperty.create("selectedElement.date_end");
		BeanProperty<JTextField, String> jTextFieldBeanProperty_3 = BeanProperty.create("text");
		AutoBinding<JTable, String, JTextField, String> autoBinding_3 = Bindings.createAutoBinding(UpdateStrategy.READ_WRITE, mastertablesc, jTableBeanProperty_3, txtdateend, jTextFieldBeanProperty_3);
		autoBinding_3.bind();
		//
		BeanProperty<JTable, String> jTableBeanProperty_4 = BeanProperty.create("selectedElement.date_begin");
		BeanProperty<JTextField, String> jTextFieldBeanProperty_4 = BeanProperty.create("text");
		AutoBinding<JTable, String, JTextField, String> autoBinding_4 = Bindings.createAutoBinding(UpdateStrategy.READ_WRITE, mastertablesc, jTableBeanProperty_4, txtdatebegin, jTextFieldBeanProperty_4);
		autoBinding_4.bind();
		//
		BeanProperty<JTable, Integer> jTableBeanProperty_5 = BeanProperty.create("selectedElement.idschedule");
		BeanProperty<JTextField, String> jTextFieldBeanProperty_5 = BeanProperty.create("text");
		AutoBinding<JTable, Integer, JTextField, String> autoBinding_5 = Bindings.createAutoBinding(UpdateStrategy.READ_WRITE, mastertablesc, jTableBeanProperty_5, textidschedule, jTextFieldBeanProperty_5);
		autoBinding_5.bind();
	}
}
