 package edu.app.GUI;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Schedule;
import javax.ejb.Schedules;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.beansbinding.AutoBinding;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.beansbinding.Bindings;
import org.jdesktop.observablecollections.ObservableCollections;
import org.jdesktop.swingbinding.JTableBinding;
import org.jdesktop.swingbinding.SwingBindings;

import edu.app.delegate.CreditServiceDelegate;
import edu.app.delegate.Schedule_creditServiceDelegate;
import edu.app.persistence.Credit;
import edu.app.persistence.Schedule_credit;
import edu.app.services.*;
import edu.app.util.Locator;
import edu.app.util.ServiceLocator;

import java.awt.Font;
public class CreditManagementForm extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4343023344498640150L;

	List<Credit> credits;
	List<Schedule_credit> schedules;
	private JPanel contentPane;
	private JTable mastertablecr;
	private JTextField textField;
	private JTextField textField_1;
	private JComboBox comboBox;
	private JLabel labNumber;
	private JTextField txtNumber;
	   CreditRemote remote;
       Schedule_creditRemote remote2;



	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CreditManagementForm frame = new CreditManagementForm();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		}

	/**
	 * Create the frame.
	 */
	public CreditManagementForm() {
	    remote = (CreditRemote) ServiceLocator.getInstance().getRemoteInterface("ejb:/espritbank/CreditImpl!edu.app.services.CreditRemote");

       remote2 = (Schedule_creditRemote) ServiceLocator.getInstance().getRemoteInterface("ejb:/espritbank/Schedule_creditImpl!edu.app.services.Schedule_creditRemote");    
      
		credits = ObservableCollections.observableList (CreditServiceDelegate.findallcredit());
		setTitle("Credit Management");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 476, 597);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JPanel masterpanel = new JPanel();
		masterpanel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Credits", TitledBorder.LEADING, TitledBorder.TOP, null, Color.BLUE));
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Credit Details", TitledBorder.LEADING, TitledBorder.TOP, null, Color.BLUE));
		
		JPanel panel_1 = new JPanel();
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(masterpanel, GroupLayout.DEFAULT_SIZE, 430, Short.MAX_VALUE)
						.addComponent(panel, GroupLayout.PREFERRED_SIZE, 428, GroupLayout.PREFERRED_SIZE)
						.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 429, GroupLayout.PREFERRED_SIZE))
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(masterpanel, GroupLayout.PREFERRED_SIZE, 257, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 183, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(25, Short.MAX_VALUE))
		);
		  comboBox = new JComboBox();
          comboBox.setToolTipText("");
          comboBox.setBounds(107, 230, 265, 20);
          schedules = remote2.findallschedule();
          for (Schedule_credit s : schedules) {

                  comboBox.addItem(s.getIdschedule() +" "+ s.getDate_begin());
          }

          panel.add(comboBox);
	
		
	      JButton btnNewButton = new JButton("New ");
	    	btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Credit credit = new Credit();
				credits.add(credit);
				int row = credits.size() - 1;
				mastertablecr.setRowSelectionInterval(row, row);
				mastertablecr.scrollRectToVisible(mastertablecr.getCellRect(row, 0, true));
			}
		});
		
		JButton btnNewButton_1 = new JButton("Load");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				List<Credit> loaded =CreditServiceDelegate.findallcredit();
				credits.clear();
				credits.addAll(loaded);
			}
		
		});
		
		JButton btnNewButton_2 = new JButton("Delete");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int[] selected = mastertablecr.getSelectedRows();
				List<Credit> toremove =new ArrayList<Credit>(selected.length);
				for(int i=0;i<selected.length;i++){
					Credit cred = credits.get(mastertablecr.convertRowIndexToModel(i));
					toremove.add(cred);
				}
				for(Credit cred : toremove){
					CreditServiceDelegate.deletecredit(cred);
				}
				credits.removeAll(toremove);
			}
			
		});
			
		
		
		JButton btnNewButton_3 = new JButton("Save");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
				for (Credit c : credits)
				{
					CreditServiceDelegate.updatecredit(c);
			}
						}
					
					});
		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_panel_1.createSequentialGroup()
					.addContainerGap(82, Short.MAX_VALUE)
					.addComponent(btnNewButton)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(btnNewButton_1)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(btnNewButton_2)
					.addGap(18)
					.addComponent(btnNewButton_3)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(btnExit, GroupLayout.PREFERRED_SIZE, 57, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		gl_panel_1.setVerticalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnExit)
						.addComponent(btnNewButton_3)
						.addComponent(btnNewButton_2)
						.addComponent(btnNewButton_1)
						.addComponent(btnNewButton))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		panel_1.setLayout(gl_panel_1);
		
		JLabel lblNewLabel = new JLabel("ID Credit");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 10));
		
		JLabel lblNewLabel_1 = new JLabel("Amount Credit");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 10));
		
		JLabel lblCreditType = new JLabel("Schedule");
		lblCreditType.setFont(new Font("Tahoma", Font.BOLD, 10));
		
		textField = new JTextField();
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		
		
		
		
		
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(lblNewLabel)
						.addComponent(lblNewLabel_1)
						.addComponent(lblCreditType))
					.addGap(22)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING, false)
						.addComponent(comboBox, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(textField, GroupLayout.DEFAULT_SIZE, 288, Short.MAX_VALUE)
						.addComponent(textField_1))
					.addContainerGap(27, Short.MAX_VALUE))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(21)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel)
						.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(27)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_1)
						.addComponent(textField_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(29)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblCreditType)
						.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(64, Short.MAX_VALUE))
		);
		panel.setLayout(gl_panel);
		
		JScrollPane masterscrollPane = new JScrollPane();
		GroupLayout gl_masterpanel = new GroupLayout(masterpanel);
		gl_masterpanel.setHorizontalGroup(
			gl_masterpanel.createParallelGroup(Alignment.TRAILING)
				.addComponent(masterscrollPane, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 399, Short.MAX_VALUE)
		);
		gl_masterpanel.setVerticalGroup(
			gl_masterpanel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_masterpanel.createSequentialGroup()
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(masterscrollPane, GroupLayout.PREFERRED_SIZE, 218, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		
		mastertablecr = new JTable();
		masterscrollPane.setViewportView(mastertablecr);
		masterpanel.setLayout(gl_masterpanel);
		contentPane.setLayout(gl_contentPane);
		initDataBindings();
			
			}
	protected void initDataBindings() {
		JTableBinding<Credit, List<Credit>, JTable> jTableBinding = SwingBindings.createJTableBinding(UpdateStrategy.READ_WRITE, credits, mastertablecr);
		//
		BeanProperty<Credit, Integer> creditBeanProperty = BeanProperty.create("idcredit");
		jTableBinding.addColumnBinding(creditBeanProperty).setColumnName("Id credit");
		//
		BeanProperty<Credit, Double> creditBeanProperty_1 = BeanProperty.create("amountcredit");
		jTableBinding.addColumnBinding(creditBeanProperty_1).setColumnName("Amount Credit");
		//
		BeanProperty<Credit, Schedule_credit> creditBeanProperty_2 = BeanProperty.create("schedule");
		jTableBinding.addColumnBinding(creditBeanProperty_2).setColumnName("Schedule");
		//
		jTableBinding.bind();
		//
		BeanProperty<JTable, Integer> jTableBeanProperty = BeanProperty.create("selectedElement.idcredit");
		BeanProperty<JTextField, String> jTextFieldBeanProperty = BeanProperty.create("text");
		AutoBinding<JTable, Integer, JTextField, String> autoBinding = Bindings.createAutoBinding(UpdateStrategy.READ_WRITE, mastertablecr, jTableBeanProperty, textField, jTextFieldBeanProperty);
		autoBinding.bind();
		//
		BeanProperty<JTable, Double> jTableBeanProperty_1 = BeanProperty.create("selectedElement.amountcredit");
		BeanProperty<JTextField, String> jTextFieldBeanProperty_1 = BeanProperty.create("text");
		AutoBinding<JTable, Double, JTextField, String> autoBinding_1 = Bindings.createAutoBinding(UpdateStrategy.READ_WRITE, mastertablecr, jTableBeanProperty_1, textField_1, jTextFieldBeanProperty_1);
		autoBinding_1.bind();
		//
		BeanProperty<JTable, String> jTableBeanProperty_2 = BeanProperty.create("selectedElement.type");
		BeanProperty<JComboBox, String> jComboBoxBeanProperty = BeanProperty.create("toolTipText");
		AutoBinding<JTable, String, JComboBox, String> autoBinding_2 = Bindings.createAutoBinding(UpdateStrategy.READ_WRITE, mastertablecr, jTableBeanProperty_2, comboBox, jComboBoxBeanProperty);
		autoBinding_2.bind();
		//
		BeanProperty<JTable, Schedule_credit> jTableBeanProperty_3 = BeanProperty.create("selectedElement.schedule");
		AutoBinding<JTable, Schedule_credit, JComboBox, String> autoBinding_3 = Bindings.createAutoBinding(UpdateStrategy.READ, mastertablecr, jTableBeanProperty_3, comboBox, jComboBoxBeanProperty);
		autoBinding_3.bind();
		//
		AutoBinding<JTable, Schedule_credit, JComboBox, String> autoBinding_4 = Bindings.createAutoBinding(UpdateStrategy.READ, mastertablecr, jTableBeanProperty_3, comboBox, jComboBoxBeanProperty);
		autoBinding_4.bind();
	}
}
