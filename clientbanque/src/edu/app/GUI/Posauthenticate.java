package edu.app.GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import java.awt.Font;
import javax.swing.JButton;

import edu.app.delegate.AuthenticationServiceDelegate;
import edu.app.delegate.CreditCardServiceDelegate;
import edu.app.persistence.Account;
import edu.app.persistence.CreditCard;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

public class Posauthenticate extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6575548255499714251L;
	private JPanel contentPane;
	private JTextField textcreditnumber;
	private JPasswordField txtpassword;
	private JTextField txtamountpos;
	  public JTextField getCardField()
      {
              return textcreditnumber;
      }
      
      public JPasswordField getCode()
      {
              return txtpassword;
      }
      

      @Override
      public String toString() {
              return "PosAuthenticate [contentPane=" + contentPane + ", code="
                              + txtpassword + ", cardField=" + textcreditnumber + "]";
      }

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Posauthenticate frame = new Posauthenticate();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	 public void printMessage(String obj, String text, int type) {
         JOptionPane.showMessageDialog(this, text, obj, type);
 }

	/**
	 * Create the frame.
	 */
	public Posauthenticate() {
		setTitle("Pos Authenticate");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 514, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		
		JButton btnNewButton = new JButton("Validate");
		btnNewButton.addActionListener(new ActionListener() {
				 public void actionPerformed(ActionEvent e) {
                     @SuppressWarnings({ "unused", "deprecation" })
                     final CreditCard creditcard = AuthenticationServiceDelegate
                                     .authenticateCreditCard(textcreditnumber.getText(),
                                                     txtpassword.getText());
     
                     if(creditcard == null)
                     {
                             printMessage("Invalid Code",
                                             " Wrong code! ",
                                             JOptionPane.ERROR_MESSAGE);
                             
                     }
                     else
                     {
                             
                             creditcard.toString();
                             Posauthenticate.this.dispose();
                             EventQueue.invokeLater(new Runnable() {
                                     public void run() {
                                             try {
                                 				float a =Float.parseFloat(txtamountpos.getText());
                                				 final CreditCard creditcard = CreditCardServiceDelegate.findCreditCardByNumber(textcreditnumber.getText());
                                                 final Account acc = CreditCardServiceDelegate.findAccountByCreditCard(creditcard);
                                                 if(acc.getBalance()<a)
                                                 {
                                                	 printMessage("Invalid Amount",
                                                             " You can't make this payement your account amount is : "+acc.getBalance(),
                                                             JOptionPane.ERROR_MESSAGE);
                                                	 
                                                 }
                                                 else
                                                 {
                                                	 float s =acc.getBalance()-a;
                                                	 acc.setBalance(s);
                                                	  printMessage("Success",
                                                              " Payement effectued your account amount is : "+acc.getBalance(),
                                                              JOptionPane.INFORMATION_MESSAGE);
                                                	 
                                                	 
                                                 }
                                                     
                                                     creditcard.toString();
                                             } catch (Exception e) {
                                                     e.printStackTrace();
                                             }
                                     }
                             });
                     }
                     

             }
     
			
		});
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addComponent(panel, GroupLayout.PREFERRED_SIZE, 335, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnNewButton))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 171, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(btnNewButton)
					.addContainerGap(235, Short.MAX_VALUE))
		);
		
		JLabel lblNewLabel = new JLabel("Credit Card Number");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 11));
		
		JLabel lblNewLabel_1 = new JLabel("Password");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 11));
		
		textcreditnumber = new JTextField();
		textcreditnumber.setColumns(10);
		
		txtpassword = new JPasswordField();
		
		JLabel lblNewLabel_2 = new JLabel("Amount to pay");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 11));
		
		txtamountpos = new JTextField();
		txtamountpos.setColumns(10);
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(lblNewLabel)
						.addComponent(lblNewLabel_1)
						.addComponent(lblNewLabel_2))
					.addGap(18)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(txtamountpos, GroupLayout.DEFAULT_SIZE, 171, Short.MAX_VALUE)
						.addComponent(txtpassword)
						.addComponent(textcreditnumber, GroupLayout.DEFAULT_SIZE, 171, Short.MAX_VALUE))
					.addGap(26))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(19)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel)
						.addComponent(textcreditnumber, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED, 43, Short.MAX_VALUE)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_1)
						.addComponent(txtpassword, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(30)
					.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
						.addComponent(lblNewLabel_2)
						.addComponent(txtamountpos, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(25))
		);
		panel.setLayout(gl_panel);
		contentPane.setLayout(gl_contentPane);
	}
}
