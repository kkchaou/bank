package edu.app.util;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
public class Locator {
@SuppressWarnings("rawtypes")
public static Object lookup(String serviceName,Class serviceViewClass){
Context context;
Object o=null;
try {
context = new InitialContext();
String viewClass=serviceViewClass.getCanonicalName();
o= context.lookup("espritbank//"+serviceName+"!"+viewClass);
} catch (NamingException e) {
// TODO Auto-generated catch block
e.printStackTrace();
}
return o;
}
}
