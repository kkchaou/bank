package edu.app.delegate;



	import java.util.List;

import edu.app.persistence.Credit;
import edu.app.services.CreditRemote;
import edu.app.util.ServiceLocator;

	public class CreditServiceDelegate {		
		private static String jndiName = "ejb:/espritbank/CreditImpl!edu.app.services.CreditRemote";
		private static CreditRemote getRemote()
		{
		return (CreditRemote) ServiceLocator.getInstance().getRemoteInterface(jndiName);
		}
		public static void createcredit(Credit credit) {
		getRemote().createcredit(credit);
		}
		public static Credit findcreditbyid(int idcredit){
		return	getRemote().findcreditbyid(idcredit);
		}
		public static void updatecredit(Credit credit) {
		getRemote().updatecredit(credit);
		}
		public static void deletecredit(Credit credit) {
		getRemote().deletecredit(credit);

		}

		public static List<Credit> findallcredit() {
		return getRemote().findallcredit();
		}


	}


