package edu.app.delegate;

import java.util.List;


import edu.app.persistence.Pos;
import edu.app.services.PosRemote;
import edu.app.util.ServiceLocator;

public class PosServiceDelegate {
			
		private static String jndiName = "ejb:/espritbank/PosImpl!edu.app.services.PosRemote";
		private static PosRemote getRemote()
		{
		return (PosRemote) ServiceLocator.getInstance().getRemoteInterface(jndiName);
		}
		public static void createpos(Pos p) {
		getRemote().createpos(p);
		}
		public static Pos findposbyid(int idpos){
		return	getRemote().findposbyid(idpos);
		}
		public static void updatepos(Pos p) {
		getRemote().updatepos(p);
		}
		public static void deletecredit(Pos p) {
		getRemote().deletepos(p);

		}

		public static List<Pos> findallpos() {
		return getRemote().findallpos();
		}


	}





