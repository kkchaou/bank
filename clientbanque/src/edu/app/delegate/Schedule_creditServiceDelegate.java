package edu.app.delegate;
import java.util.List;

import edu.app.persistence.Schedule_credit;
import edu.app.services.Schedule_creditRemote;
import edu.app.util.ServiceLocator;

public class Schedule_creditServiceDelegate {		
	private static String jndiName = "ejb:/espritbank/Schedule_creditImpl!edu.app.services.Schedule_creditRemote";
	private static Schedule_creditRemote getRemote()
	{
	return (Schedule_creditRemote) ServiceLocator.getInstance().getRemoteInterface(jndiName);
	}
	public static void createschedule(Schedule_credit schedule) {
	getRemote().createschedule(schedule);
	}
	public static Schedule_credit findschedulebyid(int idschedule){
	return	getRemote().findschedulebyid(idschedule);
	}
	public static void updateschedule(Schedule_credit schedule) {
	getRemote().updateschedule(schedule);
	}
	public static void deleteschedule(Schedule_credit schedule) {
	getRemote().deleteschedule(schedule);

	}

	public static List<Schedule_credit> findallschedule() {
	return getRemote().findallschedule();
	}
}
