package edu.app.delegate;

import java.util.List;

import edu.app.persistence.Offer;
import edu.app.services.OfferRemote;
import edu.app.util.ServiceLocator;

public class OfferServiceDelegate {
	
	private static String jndiName = "ejb:/espritbank/OfferImpl!edu.app.services.OfferRemote";
	private static OfferRemote getRemote()
	{
	return (OfferRemote) ServiceLocator.getInstance().getRemoteInterface(jndiName);
	}
	public static void createoffer(Offer offer) {
	getRemote().createoffer(offer);
	}
	public Offer findofferbyid(int idoffer){
	return	getRemote().findofferbyid(idoffer);
	}
	public static void updateoffer(Offer offer) {
	getRemote().updateoffer(offer);
	}
	public static void deleteoffer(Offer offer) {
	getRemote().deleteoffer(offer);

	}
	public static List<Offer> findalloffer() {
	return getRemote().findalloffer();
	}


}
