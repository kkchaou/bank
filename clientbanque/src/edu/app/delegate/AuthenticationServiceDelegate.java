package edu.app.delegate;


import java.util.List;

import edu.app.services.AuthenticateRemote;
import edu.app.services.CreditRemote;
import edu.app.util.Locator;
import edu.app.util.ServiceLocator;
import edu.app.persistence.CreditCard;
import edu.app.persistence.User;



public class AuthenticationServiceDelegate {
        
        private static final String jndiName = "ejb:/espritbank/Authenticate!edu.app.services.AuthenticateRemote";
        private static AuthenticateRemote getRemote()
		{
		return (AuthenticateRemote) ServiceLocator.getInstance().getRemoteInterface(jndiName);
		}
        public static void createUser(User user) {
    		getRemote().createUser(user);

        }
                
        public static List<User> findAllUsers() {
                return getRemote().findAllUsers();
        }

        public static User authenticate(String login, String password) {
              return getRemote().authenticate(login, password);
        }
        
        
        public static User authenticateAdmin(String login, String password) {
                return getRemote().authenticateAdmin(login, password);
        }
        
        
        public static User authenticateBanker(String login, String password) {
                return getRemote().authenticateBanker(login, password);
        }

        public static CreditCard authenticateCreditCard(String numCard, String codeCard){
                return getRemote().authenticateCreditCard(numCard, codeCard);
        }
}
