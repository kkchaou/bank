package edu.app.delegate;

import edu.app.persistence.Account;
import edu.app.persistence.CreditCard;
import edu.app.services.AuthenticateRemote;
import edu.app.services.CreditCardServiceRemote;
import edu.app.util.ServiceLocator;

public class CreditCardServiceDelegate {
	private static final String jndiName = "ejb:/espritbank/CreditCardServiceImpl!edu.app.services.CreditCardServiceRemote";
    private static CreditCardServiceRemote getRemote()
	{
	return (CreditCardServiceRemote) ServiceLocator.getInstance().getRemoteInterface(jndiName);
	}
	public void createCreditCard(CreditCard creditCard) {
		getRemote().createCreditCard(creditCard);
		
	}
	public void removeCreditCard(CreditCard creditCard){
		getRemote().removeCreditCard(creditCard);
	}
	public String generateCreditCardNumber() {
		return getRemote().generateCreditCardNumber();
		
	}
	public static  CreditCard findCreditCardByNumber(String number){
		return getRemote().findCreditCardByNumber(number);
	}
	public static Account findAccountByCreditCard(CreditCard creditcard){
		return getRemote().findAccountByCreditCard(creditcard);
	}



}
